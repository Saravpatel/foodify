# Foodyfi

## Introduction

* Developed a responsive web application for ordering the food online, simulated the user's data into the database with MD5 and SHA authorization.
* Found lots of critical issues in several online ordering systems and we overcame security issues, as well as user-friendly UI/UX for easy access.
* We provide a facility for advanced table booking system in various restaurants for special occasions.
---

## Technology Listing:

* Node.js
* JavaScript
* MongoDB
* HTML
* JQuery
* CSS




---

## Required Softwears

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/
* MongoDB - https://www.mongodb.com/try/download/community

---

## Setup

### Installation process 

```
npm install
```

### Start Server

```
npm server
```

### Initialization step for package.json file

```
npm init
```

### Run Application

```
node home.ejs
```

### Access Web Browser

```
http://localhost:3000/
```
---

## License

Copyright 2020 @Sarav Patel
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
